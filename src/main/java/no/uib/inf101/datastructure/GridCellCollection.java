package no.uib.inf101.datastructure;

import java.util.List;

public interface GridCellCollection<T> {
  
  /**
   * Get a list containing the GridCell<T> objects in this collection
   *
   * @return a list of all GridCell<T> objects in this collection
   */
  List<GridCell<T>> getCells(); // Oppdater til å bruke generisk type T

}

